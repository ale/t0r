package t0r

import (
	"bufio"
	"bytes"
	"encoding/gob"
	"encoding/binary"
	"io"
	"log"
	"os"
	"net/http"
	"time"
)

type LogRecord struct {
	Ip string
	UserAgent string
	Referer string
	LinkId string
	Target string
	Timestamp int64
}

func writeRecord(w io.Writer, rec *LogRecord) error {
	var buf bytes.Buffer
	if err := gob.NewEncoder(&buf).Encode(rec); err != nil {
		return err
	}
	data := buf.Bytes()
	sz := len(data)

	var hdr bytes.Buffer
	if err := binary.Write(&hdr, binary.LittleEndian, sz); err != nil {
		return err
	}

	if _, err := w.Write(hdr.Bytes()); err != nil {
		return err
	}
	_, err := w.Write(data)
	return err
}

type Logger struct {
	ch chan *LogRecord
	w io.Writer
}

func NewLogger(path string) *Logger {
	file, err := os.OpenFile(path, os.O_APPEND|os.O_CREATE, 0644)
	if err != nil {
		log.Fatal(err)
	}

	return &Logger{
		ch: make(chan *LogRecord, 50),
		w: bufio.NewWriter(file),
	}
}

func (l *Logger) Log(linkId, target string, req *http.Request) {
	rec := LogRecord{
		LinkId: linkId,
		Target: target,
		Ip: addrToHost(req.RemoteAddr),
		UserAgent: req.Header.Get("User-Agent"),
		Referer: req.Header.Get("Referer"),
		Timestamp: time.Now().Unix(),
	}
	l.ch <- &rec
}

func (l *Logger) Run() {
	for rec := range l.ch {
		writeRecord(l.w, rec)
	}
}


