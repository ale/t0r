package t0r

import (
	"flag"
	"log"
	"net/http"
	"strings"

	"github.com/bradfitz/gomemcache/memcache"
)

var (
	dosKeyNamespace = "dos"

	memcacheAddr = flag.String("memcache", "127.0.0.1:11211", "Memcache address")
	whitelistedAddrs = flag.String("whitelist-addrs", "127.0.0.1", "Whitelisted addresses (no rate limiting), comma separated")
)

type Limiter struct {
	Param string
	Count int
	Period int

	mc *memcache.Client
	whitelistedValues []string
	extractValue func(*http.Request) string
}

func NewLimiter(param string, count int, period int, extractfn func(*http.Request) string) *Limiter {
	return &Limiter{
		Param: param,
		Count: count,
		Period: period,
		extractValue: extractfn,
		whitelistedValues: strings.Split(*whitelistedAddrs, ","),
		mc: memcache.New(*memcacheAddr),
	}
}

func (l *Limiter) AddToWhitelist(value string) {
	l.whitelistedValues = append(l.whitelistedValues, value)
}

func (l *Limiter) isWhitelisted(value string) bool {
	for _, w := range l.whitelistedValues {
		if w == value {
			return true
		}
	}
	return false
}

// Limit returns true if the request is approved.
func (l *Limiter) Limit(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		value := l.extractValue(r)
		key := strings.Join([]string{dosKeyNamespace, r.Method, r.URL.Path, l.Param, value}, "|")

		result, err := l.mc.Increment(key, 1)
		if err != nil {
			result = 1
			if err = l.mc.Add(&memcache.Item{Key: key, Value: []byte("1"), Expiration: int32(l.Period)}); err != nil {
				// Possible race for who adds to the cache first.
				if result, err = l.mc.Increment(key, 1); err != nil {
					log.Printf("Memcache failed for '%s': %s", key, err)
					result = 0
				}
			}
		}

		if !l.isWhitelisted(value) && result > uint64(l.Count) {
			http.Error(w, "Rate Limited", http.StatusServiceUnavailable)
			return
		}

		handler.ServeHTTP(w, r)
	})
}

