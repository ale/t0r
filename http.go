package t0r

import (
	"crypto/tls"
	"flag"
	"fmt"
	// "html/template"
	"io"
	"net"
	"log"
	"math/rand"
	"net/http"
	"net/url"
	"strings"
	"time"
)

var (
	baseUrl = flag.String("base-url", "http://t0r.me", "Base URL for public links")

	torWebServices = []string{
		"tor2web.org",
		"onion.to",
	}
)

const indexHtml = `<!DOCTYPE html>
<html>
<head>
 <title>t0r.me</title>
 <meta name="robots" content="NONE,NOARCHIVE">
 <link rel="shortcut icon" type="image/png" href="/static/favicon.png">
 <link rel="stylesheet" type="text/css" href="/static/style.css">
</head>
<body>
 <div id="container">
  <header>
   <h1>t0r.me
    <span>a URL shortener for .onion links</span>
   </h1>
  </header>
  <div id="main">
   <a href="javascript:void(location.href='https://t0r.me/add?url='+escape(document.URL))">bookmarklet</a>
  </div>
  <footer>
   (c) 2013
  </footer>
 </div>
</body>
</html>`

// var indexPage = template.Must(template.New("index").Parse(indexHtml))

type Server struct {
	db *Database
	statsLogger *Logger
}

func addrToHost(a string) string {
	return strings.Split(a, ":")[0]
}

func (s *Server) serveAdd(w http.ResponseWriter, r *http.Request) {
	url := r.FormValue("url")
	if url == "" {
		http.Error(w, "Bad Request", http.StatusBadRequest)
		return
	}
	if !isOnion(url) {
		http.Error(w, "Not an onion URL", http.StatusBadRequest)
		return
	}

	e := Entry{url, addrToHost(r.RemoteAddr), time.Now().Unix()}
	id, err := s.db.Put(&e)
	if err != nil {
		http.Error(w, "Not Found", http.StatusNotFound)
		return
	}
	shortUrl := fmt.Sprintf("%s/%s", *baseUrl, id)
	http.Redirect(w, r, shortUrl, 302)
}

func (s *Server) serveGet(w http.ResponseWriter, r *http.Request) {
	id := r.URL.Path[1:]
	e, err := s.db.Get(id)
	if err != nil {
		http.Error(w, "Not Found", http.StatusNotFound)
		return
	}
	url := pickUrl(e)
	http.Redirect(w, r, url, 302)
	s.statsLogger.Log(id, url, r)
}

func isOnion(s string) bool {
	parsedUrl, _ := url.Parse(s)
	return strings.HasSuffix(parsedUrl.Host, ".onion")
}

func pickUrl(e *Entry) string {
	parsedUrl, _ := url.Parse(e.Url)
	onion := parsedUrl.Host[:len(parsedUrl.Host)-6]
	ws := torWebServices[rand.Intn(len(torWebServices))]
	parsedUrl.Host = fmt.Sprintf("%s.%s", onion, ws)
	return parsedUrl.String()
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path == "/add" {
		s.serveAdd(w, r)
		return
	}
	if len(r.URL.Path) == 11 {
		s.serveGet(w, r)
		return
	}
	if r.URL.Path == "/" {
		io.WriteString(w, indexHtml)
		// indexPage.Execute(w, struct{}{})
		return
	}
	http.Error(w, "Not Found", http.StatusNotFound)
}

func NewServer(db *Database, statsLogger *Logger, staticRoot string) http.Handler {
	mux := http.NewServeMux()
	mux.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir(staticRoot))))
	mux.HandleFunc("/robots.txt", func(w http.ResponseWriter, r *http.Request) {
		io.WriteString(w, "User-Agent: *\nDisallow: /\n")
	})
	mux.Handle("/", &Server{db, statsLogger})

	limiter := NewLimiter("ip", 10, 1, func(r *http.Request) string {
		return addrToHost(r.RemoteAddr)
	})
	return limiter.Limit(mux)
}

func Log(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Printf("%s %s %s", addrToHost(r.RemoteAddr), r.Method, r.URL)
		handler.ServeHTTP(w, r)
	})
}

func SetServerHeaders(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Server", "t0r")
		w.Header().Set("X-Frame-Options", "SAMEORIGIN")
		handler.ServeHTTP(w, r)
	})
}

func ListenAndServe(server http.Handler, l net.Listener, addr string) {
	h := Log(SetServerHeaders(server))
	httpServer := &http.Server{
		Addr: addr,
		Handler: h,
		ReadTimeout: 10 * time.Second,
		WriteTimeout: 10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	log.Fatal(httpServer.Serve(l))
}

func ListenAndServeTLS(server http.Handler, l net.Listener, addr, certFile, keyFile string) {
	h := Log(SetServerHeaders(server))
	httpServer := &http.Server{
		Addr: addr,
		Handler: h,
		ReadTimeout: 10 * time.Second,
		WriteTimeout: 10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}
	tlsconf := &tls.Config{
		Certificates: make([]tls.Certificate, 1),
		CipherSuites: []uint16{
			tls.TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA,
			tls.TLS_ECDHE_RSA_WITH_RC4_128_SHA,
			tls.TLS_RSA_WITH_AES_128_CBC_SHA,
			tls.TLS_RSA_WITH_RC4_128_SHA,
		},
	}

	var err error
	tlsconf.Certificates[0], err = tls.LoadX509KeyPair(certFile, keyFile)
	if err != nil {
		log.Fatal(err)
	}

	tlsListener := tls.NewListener(l, tlsconf)
	log.Fatal(httpServer.Serve(tlsListener))
}

