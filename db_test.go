package t0r

import (
	"log"
	"os"
	"testing"
)

type testDbCtx struct {
	db *Database
}

func newTestDbCtx() *testDbCtx {
	return &testDbCtx{NewDatabase("__test_db")}
}

func (ctx *testDbCtx) Close() {
	ctx.db.Close()
	os.RemoveAll("__test_db")
}

func TestRngSeeder_Next(t *testing.T) {
	ctx := newTestDbCtx()
	defer ctx.Close()

	s := NewRngSeeder(ctx.db.db)
	id := s.Next()
	if id == 0 {
		t.Fatal("id was 0")
	}
}

func TestRngSeeder_Write(t *testing.T) {
	ctx := newTestDbCtx()
	defer ctx.Close()

	for i := 0; i < 10; i++ {
		e := Entry{"xyzxyz.onion", "1.2.3.4", 1234}
		id, err := ctx.db.Put(&e)
		if err != nil {
			t.Errorf("Put() error: %s", err)
		}
		log.Printf("%s", id)
	}
}
