package t0r

import (
	"bytes"
	"log"
	"encoding/base64"
	"encoding/binary"
	"encoding/gob"
	"math/rand"
	"sync"

	"github.com/jmhodges/levigo"
)

const (
	seedKey = "@seed"
)

type Entry struct {
	Url string
	RemoteAddr string
	Timestamp int64
}

type Seeder interface {
	Next() int64
}

type Database struct {
	db *levigo.DB
	cache *levigo.Cache
	filter *levigo.FilterPolicy

	seeder Seeder
}

func NewDatabase(path string) *Database {
	opts := levigo.NewOptions()
	cache := levigo.NewLRUCache(2 << 28)
	opts.SetCache(cache)
	opts.SetCreateIfMissing(true)
	filter := levigo.NewBloomFilter(12)
	opts.SetFilterPolicy(filter)
	db, err := levigo.Open(path, opts)
	if err != nil {
		log.Fatal("Error creating database: %s", err)
	}
	return &Database{db, cache, filter, NewRngSeeder(db)}
}

func (d *Database) Close() {
	d.db.Close()
	d.cache.Close()
	d.filter.Close()
}

func (d *Database) readRaw(key string) ([]byte, error) {
	ro := levigo.NewReadOptions()
	defer ro.Close()
	return d.db.Get(ro, []byte(key))
}

func (d *Database) Get(key string) (*Entry, error) {
	data, err := d.readRaw(key)
	if err != nil {
		return nil, err
	}
	var e Entry
	if err := gob.NewDecoder(bytes.NewBuffer(data)).Decode(&e); err != nil {
		return nil, err
	}
	return &e, nil
}

func encodeKey(id int64) string {
	var b bytes.Buffer
	// bravely ignore encoding errors! urgh...
	binary.Write(&b, binary.LittleEndian, id)
	enc := base64.URLEncoding.EncodeToString(b.Bytes())
	return enc[:10]
}

func (d *Database) getNewId() (key string) {
	for {
		next := d.seeder.Next()
		key = encodeKey(next)
		if _, err := d.readRaw(key); err == nil {
			return
		}
	}
	return
}

func (d *Database) Put(e *Entry) (string, error) {
	var b bytes.Buffer
	if err := gob.NewEncoder(&b).Encode(e); err != nil {
		return "", err
	}

	id := d.getNewId()

	wo := levigo.NewWriteOptions()
	defer wo.Close()
	return id, d.db.Put(wo, []byte(id), b.Bytes())
}

type RngSeeder struct {
	lock sync.Mutex
	rng  *Kiss64
	db   *levigo.DB
}

func NewRngSeeder(db *levigo.DB) *RngSeeder {
	s := &RngSeeder{db: db}
	s.rng = s.readState()
	return s
}

func (s *RngSeeder) readState() *Kiss64 {
	ro := levigo.NewReadOptions()
	defer ro.Close()
	data, err := s.db.Get(ro, []byte(seedKey))
	if err == nil {
		var state Kiss64
		if err := gob.NewDecoder(bytes.NewBuffer(data)).Decode(&state); err == nil {
			return &state
		}
	}
	return NewKiss()
}

func (s *RngSeeder) writeState() {
	var b bytes.Buffer
	if err := gob.NewEncoder(&b).Encode(s.rng); err != nil {
		return
	}
	wo := levigo.NewWriteOptions()
	defer wo.Close()
	s.db.Put(wo, []byte(seedKey), b.Bytes())
}

func (s *RngSeeder) Next() int64 {
	s.lock.Lock()
	defer s.lock.Unlock()
	r := rand.New(s.rng)
	defer s.writeState()
	return r.Int63()
}
