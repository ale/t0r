package main

import (
	"flag"
	"log"
	"path/filepath"
	"net"
	"os"
	"syscall"

	"git.autistici.org/ale/t0r"
)

var (
	dbPath = flag.String("db", "", "Database root dir")
	addr = flag.String("addr", ":80", "HTTP port")
	sslAddr = flag.String("ssl-addr", ":443", "HTTPS port")
	certFile = flag.String("ssl-cert", "", "SSL certificate file")
	keyFile = flag.String("ssl-key", "", "SSL private key file")
	shareDir = flag.String("share-dir", "/usr/share/t0r", "Static content dir")
	userId = flag.Int("userid", 65534, "Numeric user id to run as")
	statsLogPath = flag.String("stats-log", "linkstats.log", "Log file for statistics")
)

func main() {
	flag.Parse()

	if *dbPath == "" {
		log.Fatal("Must specify --db")
	}
	sslEnabled := (*certFile != "" && *keyFile != "")

	// Listen on privileged ports.
	conn, err := net.Listen("tcp", *addr)
	if err != nil {
		log.Fatal(err)
	}
	var tlsConn net.Listener
	if sslEnabled {
		tlsConn, err = net.Listen("tcp", *sslAddr)
		if err != nil {
			log.Fatal(err)
		}
	}

	// Drop privileges.
	if os.Getuid() == 0 {
		if err := syscall.Setuid(*userId); err != nil {
			log.Fatal("Setuid error: ", err)
		}
	}

	db := t0r.NewDatabase(*dbPath)
	staticDir := filepath.Join(*shareDir, "static")
	statsLogger := t0r.NewLogger(*statsLogPath)
	go statsLogger.Run()
	server := t0r.NewServer(db, statsLogger, staticDir)

	// Start the HTTP/HTTPS servers.
	if sslEnabled {
		go t0r.ListenAndServeTLS(server, tlsConn, *sslAddr, *certFile, *keyFile)
	}
	t0r.ListenAndServe(server, conn, *addr)
}
